import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilidadesComponent } from './utilidades.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
	{
		path: '',
    component: UtilidadesComponent
	},
];


@NgModule({
  declarations: [
    UtilidadesComponent
  ],
  imports: [
    CommonModule,
		RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UtilidadesModule { }
