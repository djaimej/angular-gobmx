import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from './layout/landing/landing.component';
import { NotFoundComponent } from './layout/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'utilidades',
    component: LandingComponent,
    loadChildren: () => import('./modules/utilidades/utilidades.module').then(m => m.UtilidadesModule)
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
		initialNavigation: 'enabled',
		relativeLinkResolution: 'legacy',
		onSameUrlNavigation: 'reload'
	})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
